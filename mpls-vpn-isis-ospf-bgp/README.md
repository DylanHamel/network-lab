# MPLS - VPN

**LDP** to exchange Labels

**IS-IS** as underlay [In Service Provider Backbone] (``L1-Only``, ``L2-Only`` & ``L1-L2``)

**BGP** as Overlay [Using vpnv4]

**OSPF** as client routing protocol [Between PE and CE]

=> Redistribution 

1. BGP in OSPF
2. OSPF in BGP

**ECMP** to have 2 active sites

**HSRP** between two CE002-00X (Need to be improve => Track)

![network](./network.png)