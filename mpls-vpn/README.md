Ping works from ``ch-zrh-ce-google-01`` to ``ch-gva-ce-google-01``
Don't forget to specify the source IP !

```shell
ch-zrh-ce-google-01#ping 192.168.2.1 source 192.168.1.1
Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 192.168.2.1, timeout is 2 seconds:
Packet sent with a source address of 192.168.1.1
!!!!!
Success rate is 100 percent (5/5), round-trip min/avg/max = 8/8/10 ms
```


![architecture.png](architecture.png)

